package strings;

import java.util.StringJoiner;
import java.util.stream.IntStream;

public class Java8_StringExamples {

    public static void main(String[] args) {

        /**
         * Example 1 create a String stream String.chars()
         */
        String stadium = "Fenerbahçe Şükrü Saraçoğu Ülker Stadyumu";
        final IntStream charStream = stadium.chars();
        charStream.mapToObj(letter -> (char) letter).map(Character::toUpperCase).forEach(System.out::print);

        /**
         * String concat types
         * s1 + s2 is easy but not efficient many intermediate operations and multiple string creation and delation occurs
         * StringBuffer is better but StringBuffer is synchronized operations
         * StringBuilder is better than StringBuffer, jdk5
         * StringJoiner is a new type to concat strings, jdk8
         */
        System.out.println();
        String s1 = "Apple";
        String s2 = "Orange";
        String s3 = "Grape";
        final String fruits1 = s1 + ", " + s2 + ", " + s3; // + operation
        System.out.println("fruits1:" + fruits1);

        StringBuffer sbf = new StringBuffer();
        final String seperator = ", ";
        final StringBuffer fruits2 = sbf.append(s1).append(seperator)
                                        .append(s2).append(seperator).append(s3); // StringBuffer
        System.out.println("fruits2:" + fruits2);

        StringBuilder sbd = new StringBuilder();
        final StringBuilder fruits3 = sbd.append(s1).append(seperator)
                                         .append(s2).append(seperator).append(s3); // StringBuilder
        System.out.println("fruits3:" + fruits3);

        StringJoiner sj = new StringJoiner(seperator);
        final String fruits4 = sj.add(s1).add(s2).add(s3).toString(); // StringJoiner is easy and efficient
        System.out.println("fruits4:" + fruits4);

        StringJoiner sj2 = new StringJoiner(seperator, "<<< ", " >>>");
        final String fruits5 = sj2.add(s1).add(s2).add(s3).toString();
        System.out.println("fruits5:" + fruits5);



    }

}
