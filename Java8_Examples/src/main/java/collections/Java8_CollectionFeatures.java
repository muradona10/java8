package collections;

import FunctionalInterfaces.model.Star;
import FunctionalInterfaces.util.FileReader;
import FunctionalInterfaces.util.FileReaderImpl;
import FunctionalInterfaces.util.ListUtils;

import java.io.InputStream;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class Java8_CollectionFeatures {

    public static void main(String[] args) {

        FileReader reader = new FileReaderImpl();
        InputStream is = Java8_CollectionFeatures.class.getClassLoader().getResourceAsStream("YesilcamYildizlari.txt");
        final List<String> lines = reader.readFile(is);
        List<Star> stars = ListUtils.getList(lines);

        final List<String> starnames = stars.stream().map(Star::getName).collect(Collectors.toList());

        // UPPERCASE ALL
        starnames.replaceAll(String::toUpperCase);
        starnames.forEach(System.out::println);
        System.out.println("-----------------------------------------------------------------------------------------");

        // REMOVE IF
        final boolean removed = starnames.removeIf(name -> name.length() > 10);
        if (removed) {
            starnames.forEach(System.out::println);
        }

        System.out.println("-----------------------------------------------------------------------------------------");

        // SORT 1, natural order means alphabetically
        starnames.sort(Comparator.naturalOrder());
        starnames.forEach(System.out::println);
        System.out.println("-----------------------------------------------------------------------------------------");

        // SORT 2, reversed
        Comparator reverse = Comparator.naturalOrder().reversed();
        starnames.sort(reverse);
        starnames.forEach(System.out::println);
        System.out.println("-----------------------------------------------------------------------------------------");

        // SORT 3, reverseOrder
        starnames.sort(Comparator.reverseOrder());
        starnames.forEach(System.out::println);
        System.out.println("-----------------------------------------------------------------------------------------");

        // SORT 4, custom comparison
        Comparator<String> cmp = (s1, s2) -> s1.length() - s2.length();
        starnames.sort(cmp);
        starnames.forEach(System.out::println);
        System.out.println("-----------------------------------------------------------------------------------------");

        // SORT 5, custom comparison 2
        System.out.println("AGE COMPARE");
        Comparator<Star> ageCmp = (s1, s2) -> {
            Integer currentYear = LocalDate.now().getYear();
            return (currentYear-s1.getBirthDate().getYear()) - (currentYear-s2.getBirthDate().getYear());
        };
        stars.sort(ageCmp);
        stars.forEach(System.out::println);
        System.out.println("-----------------------------------------------------------------------------------------");

        // comparing, thenComparing
        stars.sort(Comparator.comparing(Star::getBirthDate).thenComparing(Star::getGender).thenComparing(Star::getName));
        stars.forEach(System.out::println);

    }

}
