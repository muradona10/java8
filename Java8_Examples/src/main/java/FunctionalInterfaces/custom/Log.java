package FunctionalInterfaces.custom;

import java.util.Objects;

@FunctionalInterface
public interface Log<T> {

    String print(T t);

    default Log<T> concat(Log<T> other) {
        Objects.requireNonNull(other);
        return (t) -> this.print(t).concat(other.print(t));
    }

    static void printMe(){
        System.out.println("I am the static method of Functional Interface: Log");
    }

}
