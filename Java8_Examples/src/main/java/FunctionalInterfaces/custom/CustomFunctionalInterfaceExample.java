package FunctionalInterfaces.custom;

import FunctionalInterfaces.model.Gender;
import FunctionalInterfaces.model.Star;

import java.time.LocalDate;

public class CustomFunctionalInterfaceExample {

    public static void main(String[] args) {

        String s1 = "Bruce Lee";
        Integer i1 = 34;
        Star star1 = new Star();
        star1.setName("Star1");
        star1.setGender(Gender.F);
        star1.setBirthDate(LocalDate.of(1987, 05, 25));

        /**
         * With an anonymous class we can identify out abstract method
         */
        Log<String> l1 = new Log<String>() {
            @Override
            public String print(String s) {
                return s1.toUpperCase();
            }
        };

        /**
         * And we can use lambda expression too
         */
        Log<Integer> l2 = (l) -> l.toString();

        /**
         * Or we can use method reference style
         */
        Log<Star> l3 = Star::toString;
        Log<String> l4 = String::toLowerCase;

        /**
         * And we can call default method
         */
        Log<String> concated = l1.concat(l4);

        System.out.println(l1);
        System.out.println(l2.print(i1));
        System.out.println(l3.print(star1));
        System.out.println(l4.print("Fenerbahçe Spor Kulubü"));
        System.out.println(concated.print(" Murat"));

        Log.printMe();

    }

}
