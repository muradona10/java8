package FunctionalInterfaces.function;

import java.util.function.BiFunction;
import java.util.function.BinaryOperator;
import java.util.function.UnaryOperator;

public class BinaryOperatorExample {

    /**
     *  BinaryOperator is a special type of BiFunction
     *  BinaryOperator<T> takes two argument T and T then return also T
     *  apply is the base method
     *  They are used commonly in the map section of streams
     */

    public static void main(String[] args) {

        BiFunction<Integer, Integer, Integer> compare = Integer::compareTo;
        BinaryOperator<Integer> compare2 = Integer::compareTo;
        BinaryOperator<Integer> sum = Integer::sum;
        BinaryOperator<String> concat = String::concat;
        BinaryOperator<Long> max = Long::max;
        BinaryOperator<String> equalsIgnoreCase = (s1,s2) -> s1.equalsIgnoreCase(s2) ? s1 : s2;
        BinaryOperator<Long> multiply = (l1, l2) -> l1 * l2;

        System.out.println(compare.apply(16,30));
        System.out.println(compare2.apply(9,9));
        System.out.println(sum.apply(1900,7));
        System.out.println(concat.apply("Fener","Bahçe"));
        System.out.println(max.apply(Long.valueOf(60),Long.valueOf(58)));
        System.out.println(equalsIgnoreCase.apply("Murat","Demir"));
        System.out.println(multiply.apply(Long.valueOf(9),Long.valueOf(9)));

    }

}
