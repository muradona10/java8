package FunctionalInterfaces.function;

import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.UnaryOperator;

public class BiFunctionExample {

    /**
     *  BiFunction<T, U, R> takes two argument T and U then return R
     *  apply is the base method
     *  They are used commonly in the map section of streams
     */

    public static void main(String[] args) {

        BiFunction<String, String, Integer> twoStringsLength = (s1, s2) -> s1.length() + s2.length();
        BiFunction<Integer, Integer, Integer> compare = Integer::compareTo; // It is also a BinaryOperator

        UnaryOperator<Integer> times2 = i1-> i1 * 2;
        UnaryOperator<Integer> square = i1 -> i1 * i1;
        BiFunction<String, Integer, Integer> stringLengthByInteger = (s, i) -> s.length() * i;

        String s1 = "Fenerbahçe";
        String s2 = "Mustafa Kemal Atatürk";
        Integer i1 = 10;
        Integer i2 = 5;

        Integer twoStringsLengthVal = twoStringsLength.apply(s1, s2);
        System.out.println("Total length of s1 and s2:" + twoStringsLengthVal);

        Integer comparedRes = compare.apply(i1, i2);
        System.out.println("Compare result of i1 and i2:" + comparedRes);

        Integer res1 = times2.apply(i1);
        Integer res2 = square.apply(i2);
        Integer res3 = times2.compose(square).apply(i1);
        Integer res4 = times2.andThen(square).apply(i1);

        Integer res5 = stringLengthByInteger.apply(s2, i1);

        System.out.println("res1:" + res1);
        System.out.println("res2:" + res2);
        System.out.println("res3:" + res3);
        System.out.println("res4:" + res4);
        System.out.println("res5:" + res5);
    }

}
