package FunctionalInterfaces.function;

import FunctionalInterfaces.constants.ProjectConstants;
import FunctionalInterfaces.model.Gender;
import FunctionalInterfaces.model.Star;
import FunctionalInterfaces.util.FileReader;
import FunctionalInterfaces.util.FileReaderImpl;
import FunctionalInterfaces.util.ListUtils;

import java.io.InputStream;
import java.time.LocalDate;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.TreeSet;
import java.util.function.Function;
import java.util.stream.Collectors;

public class FunctionExample {

    /**
     *  Function<T, R> takes an argument T and return R
     *  apply is the base method
     *  They are used commonly in the map section of streams
     */
    public static void main(String[] args) {

        FileReader reader = new FileReaderImpl();
        InputStream is = FunctionExample.class.getClassLoader().getResourceAsStream("YesilcamYildizlari.txt");
        final List<String> lines = reader.readFile(is);
        final List<Star> stars = ListUtils.getList(lines);

        /**
         * Basic Examples
         */
        Function<Integer, String> toString = integer -> String.valueOf(integer);
        Function<String, Integer> sentenceCount = s -> s.split(ProjectConstants.WORD_SEPERATOR).length;
        Function<Object, Boolean> nullCheck = Objects::isNull;

        /**
         * create new list from the name length of stars
         */
        Function<String, Integer> sentenceLength = String::length;
        List<Integer> nameLengths = stars.stream().map(s -> sentenceLength.apply(s.getName())).collect(Collectors.toList());
        nameLengths.forEach(System.out::println);

        System.out.println("------------------------------------------------------------------------------------------");

        /**
         * create new list from age of the stars
         */
        Function<Star, Integer> ageOfTheStar = s -> -1*(s.getBirthDate().getYear() - LocalDate.now().getYear());
        TreeSet<Integer> ages = stars.stream().map(s -> ageOfTheStar.apply(s)).collect(Collectors.toCollection(TreeSet::new));
        ages.forEach(System.out::println);

        System.out.println("------------------------------------------------------------------------------------------");

        /**
         *  get female and male count via map
         *  our Function is Star::getGender
         */
        Map<Gender, Long> genderMap = stars.stream()
                .map(Star::getGender)
                .collect(Collectors.groupingBy(item -> item, Collectors.counting()));
        genderMap.forEach((k,v) -> System.out.println(k + ": " + v));

        System.out.println("------------------------------------------------------------------------------------------");

        /**
         *  group by their dob year
         */
        Map<Integer, Long> dobYearsMap = stars.stream()
                .collect(Collectors.groupingBy(star -> star.getBirthDate().getYear(), Collectors.counting()));
        dobYearsMap.forEach((k,v) -> System.out.println(k + ": " + v));

        System.out.println("------------------------------------------------------------------------------------------");

        /**
         * Group by their ages and create joined strings from same ages
         */
        final Map<Integer, String> ageStarMap = stars.stream().collect(Collectors.groupingBy(
                ageOfTheStar,
                Collectors.mapping(
                        Star::getName, Collectors.joining(ProjectConstants.COLUMN, ProjectConstants.PREFIX, ProjectConstants.SUfFIX)
                )
        ));
        ageStarMap.entrySet().forEach(System.out::println);


    }



}
