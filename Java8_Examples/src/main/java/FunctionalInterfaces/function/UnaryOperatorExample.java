package FunctionalInterfaces.function;

import FunctionalInterfaces.constants.ProjectConstants;
import FunctionalInterfaces.model.Gender;
import FunctionalInterfaces.model.Star;
import FunctionalInterfaces.util.FileReader;
import FunctionalInterfaces.util.FileReaderImpl;
import FunctionalInterfaces.util.ListUtils;

import java.io.InputStream;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.function.UnaryOperator;
import java.util.stream.Collectors;

public class UnaryOperatorExample {

    /**
     * Function<T, R> takes an argument T and return R
     * UnaryOperator is a special type of Function that takes and returns same type
     * UnaryOperator<T>
     */

    public static void main(String[] args) {

        FileReader reader = new FileReaderImpl();
        InputStream is = UnaryOperatorExample.class.getClassLoader().getResourceAsStream("YesilcamYildizlari.txt");
        final List<String> lines = reader.readFile(is);
        final List<Star> stars = ListUtils.getList(lines);

        /**
         * Basic examples
         */
        UnaryOperator<String> lowerCase = String::toLowerCase;
        UnaryOperator<String> upperCase = String::toUpperCase;
        UnaryOperator<String> reverse = s -> new StringBuilder(s).reverse().toString();
        UnaryOperator<Integer> times2 = i -> i * 2;

        /**
         * Create a list via uppercase
         */
        stars.stream().map(star -> {
            return upperCase.apply(star.getName());
        }).collect(Collectors.toList()).forEach(System.out::println);

        System.out.println("------------------------------------------------------------------------------------------");

        /**
         * Create a string joiner from stars but use reversed names
         */
        String reversedStarName = stars.stream().map(star -> {
            return reverse.apply(star.getName());
        }).collect(Collectors.joining(ProjectConstants.SEMI_COLUMN, ProjectConstants.PREFIX, ProjectConstants.SUfFIX));
        System.out.println(reversedStarName);

        System.out.println("------------------------------------------------------------------------------------------");

        /**
         * Create a list from stars but use only names, lowercase and reverse
         */
        stars.stream().map(Star::getName).map(lowerCase.andThen(reverse)).collect(Collectors.toList()).forEach(System.out::println);

        System.out.println("------------------------------------------------------------------------------------------");

        /**
         * Group by Female stars by their dob month, and uppercase and reverse names
         */
        stars.stream().filter(star -> star.getGender().equals(Gender.F))
                .collect(Collectors.groupingBy(star -> star.getBirthDate().getMonth(),
                        Collectors.mapping(
                            s -> upperCase.apply(reverse.apply(s.getName())), Collectors.toList()
                ))).entrySet().forEach(System.out::println);

        System.out.println("------------------------------------------------------------------------------------------");

        /**
         * Find oldest and youngest stars
         */
        Optional<Star> oldestStar = stars.stream().min(Comparator.comparing(Star::getBirthDate));
        if (oldestStar.isPresent()){
            System.out.println("Oldest Star:" + oldestStar);
        }
        Optional<Star> youngestStar = stars.stream().max(Comparator.comparing(Star::getBirthDate));
        if (youngestStar.isPresent()){
            System.out.println("Youngest Star:" + youngestStar);
        }
    }
}
