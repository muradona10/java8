package FunctionalInterfaces.constants;

public class ProjectConstants {

    public static final String SEMI_COLUMN = ";";
    public static final String COLUMN = ", ";
    public static final String DateFormatter_1 = "dd.MM.yyyy";
    public static final String WORD_SEPERATOR = " ";
    public static final String PREFIX = "[";
    public static final String SUfFIX = "]";

}
