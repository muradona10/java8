package FunctionalInterfaces.consumer;

import FunctionalInterfaces.model.Gender;
import FunctionalInterfaces.model.Star;
import FunctionalInterfaces.util.FileReader;
import FunctionalInterfaces.util.FileReaderImpl;
import FunctionalInterfaces.util.ListUtils;

import java.io.InputStream;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.BiConsumer;
import java.util.function.Consumer;

public class ConsumerExample {

    /*
    Consumer is a simple Functional Interface from java.util.function
    That takes and argument and return void
    Simple example is System.out.println(Argument);

    BiConsumer takes two parameters and returns void
     */

    public static void main(String[] args) {

        FileReader fileReader = new FileReaderImpl();
        InputStream is = ConsumerExample.class.getClassLoader().getResourceAsStream("YesilcamYildizlari.txt");
        List<String> lines = fileReader.readFile(is);

        /**
         * Example 1:
         * Just write lines
         */
        //lines.stream().forEach(System.out::println); // i -> System.out.println(i); is a lambda expression

        final List<Star> stars = ListUtils.getList(lines);
        /**
         * Example 2:
         * Just write toString for each star
         */
        //stars.forEach(System.out::println);

        /**
         * Example 3:
         * we use both Predicate and Consumer
         * Create tree list
         * first lists holds only females
         * second list holds only males
         * third list holds dateOfBirth less than 1950-01-01
         */
        List<Star> females = new ArrayList<>();
        List<Star> males = new ArrayList<>();
        List<Star> lessThan1950 = new ArrayList<>();
        Consumer<Star> addFemaleListConsumer = item -> females.add(item); // Lambda Expression
        Consumer<Star> addMaleListConsumer = males::add;  // Method Reference
        Consumer<Star> addLessThan1950 = new Consumer<Star>() {  // Anonymous Class
            @Override
            public void accept(Star star) {
                lessThan1950.add(star);
            }
        };

        stars.stream().filter(star -> star.getGender().equals(Gender.F)).forEach(addFemaleListConsumer);
        stars.stream().filter(star -> star.getGender().equals(Gender.M)).forEach(addMaleListConsumer);
        stars.stream().filter(star -> star.getBirthDate().isBefore(LocalDate.of(1950, 1, 1))).forEach(addLessThan1950);

        /**
         * BiConsumer Example
         * BiConsumer takes two objects and returns void
         * Consumer<T> return void
         * BiConsumer<T,U> return void
         *
         * Create map from stars
         */
        Map<String, Star> starMap = new HashMap<>();
        BiConsumer<Star, String> starMapBiConsumer = (s, k) -> starMap.put(k, s);
        stars.stream().forEach( star -> {
            starMapBiConsumer.accept(star, star.getName());
        });
        starMap.forEach((k, v) -> System.out.println(k + ":" + v.toString()));

    }


}
