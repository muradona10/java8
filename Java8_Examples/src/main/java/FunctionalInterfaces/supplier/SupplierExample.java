package FunctionalInterfaces.supplier;

import FunctionalInterfaces.model.Gender;
import FunctionalInterfaces.model.Star;
import FunctionalInterfaces.util.FileReader;
import FunctionalInterfaces.util.FileReaderImpl;
import FunctionalInterfaces.util.ListUtils;

import java.io.InputStream;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.function.Supplier;

public class SupplierExample {

    /*
    Supplier is a simple Functional Interface from java.util.function
    That takes no argument and return object
    They are used in orElseThrow part of streams commonly to throw an exception
     */

    public static void main(String[] args) {

        FileReader reader = new FileReaderImpl();
        InputStream is = SupplierExample.class.getClassLoader().getResourceAsStream("YesilcamYildizlari.txt");
        final List<String> lines = reader.readFile(is);

        final List<Star> stars = ListUtils.getList(lines);

        /**
         * Example 1, create exception via supplier
         * To get exception uncomment lines
         */
        Supplier noStarFound = () -> new NoSuchElementException("Star Not Founded..!");
        Star xNameStar = null;
        try {
            xNameStar = stars.stream().filter(star -> star.getName().startsWith("X"))
                    .findFirst().orElseThrow(noStarFound);
        } catch (NoSuchElementException ex){
            System.out.println(ex.getMessage());
        }
        System.out.println(xNameStar);


        /**
         * Example 2, create random variables
         */
        Supplier<Double> random = () -> Math.random() * 100;
        List<Double> myRandoms = new ArrayList<>();
        for (int i=0 ; i < 10 ; i++) {
            myRandoms.add(random.get());
        }

        /**
         * Example 3, mix
         */
        Supplier<LocalDate> localDateSupplier = LocalDate::now;
        Supplier<Instant> instantSupplier = Instant::now;
        Supplier<LocalDateTime> localDateTimeSupplier = LocalDateTime::now;
        Supplier<Double> randomSupplier = Math::random;
        Supplier<List<String>> listSupplier = ArrayList::new;
        Supplier<Star> starSupplier = () -> new Star("Star", Gender.M, LocalDate.now());

        Instant currentInstant = instantSupplier.get();
        LocalDateTime currentLocalDateTime = localDateTimeSupplier.get();
        Double myRandom = randomSupplier.get();
        List<String> strings = listSupplier.get();
        strings.add("Supplier");
        strings.add("Predicate");
        Star star = starSupplier.get();

        System.out.println("LocalDate:" + localDateSupplier);
        System.out.println("Instant:" + currentInstant);
        System.out.println("LocalDateTime:" + currentLocalDateTime);
        System.out.println("Random:" + myRandom);
        System.out.println("Strings:" + strings);
        System.out.println("Star:" +star);

    }


}
