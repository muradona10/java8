package FunctionalInterfaces.model;

import java.time.LocalDate;

public class Star {

    private String name;
    private Gender gender;
    private LocalDate birthDate;

    public Star(){}

    public Star(String name, Gender gender, LocalDate birthDate) {
        this.name = name;
        this.gender = gender;
        this.birthDate = birthDate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public LocalDate getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(LocalDate birthDate) {
        this.birthDate = birthDate;
    }

    @Override
    public String toString() {
        return "Star{" +
                "name='" + name + '\'' +
                ", gender=" + gender.getGender() +
                ", birthDate=" + birthDate +
                '}';
    }
}
