package FunctionalInterfaces.predicate;

import FunctionalInterfaces.model.Gender;
import FunctionalInterfaces.model.Star;
import FunctionalInterfaces.util.FileReader;
import FunctionalInterfaces.util.FileReaderImpl;
import FunctionalInterfaces.util.ListUtils;

import java.io.InputStream;
import java.time.LocalDate;
import java.util.List;
import java.util.function.BiPredicate;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class PredicateExample {

    /*
    Predicate is a simple Functional Interface from java.util.function
    That takes and argument and return boolean
    They are used in filter part of streams commonly

    BiPredicate takes two arguments and return boolean
     */

    public static void main(String[] args) {

        FileReader reader = new FileReaderImpl();
        InputStream is = PredicateExample.class.getClassLoader().getResourceAsStream("YesilcamYildizlari.txt");
        final List<String> lines = reader.readFile(is);

        final List<Star> stars = ListUtils.getList(lines);

        /**
         * Get only females
         */
        Predicate<Star> female = new Predicate<Star>() {
            @Override
            public boolean test(Star s) {
                return s.getGender().equals(Gender.F);
            }
        };
        List<Star> females = stars.stream().filter(female).collect(Collectors.toList());

        /**
         * Get only males
         */
        List<Star> males = stars.stream().filter(s -> s.getGender().equals(Gender.M)).collect(Collectors.toList());

        /**
         * Get only name length is greater than 15 chars
         */
        List<Star> namelengthGreaterThan15s = stars.stream().filter(star -> star.getName().length() > 15).collect(Collectors.toList());

        /**
         * Get only name starts with 'A' letter
         */
        List<Star> nameStartWihtLetter_A = stars.stream().filter(s -> s.getName().startsWith("A")).collect(Collectors.toList());

        /**
         * Get only dob is less than 1950 and Female or dob is greater than 1945 and male
         */
        Predicate<Star> male = s -> s.getGender().equals(Gender.M);
        Predicate<Star> dobLT1950 = s -> s.getBirthDate().isBefore(LocalDate.of(1950, 01, 01));
        Predicate<Star> dobGT1945 = s -> s.getBirthDate().isAfter(LocalDate.of(1945, 01, 01));
        List<Star> complex = stars.stream().filter((female.and(dobLT1950)).or(male.and(dobGT1945))).collect(Collectors.toList());

        /**
         * BiPredicate Example
         * BiPredicate takes two objects and returns boolean
         * Predicate<T> return boolean
         * BiPredicate<T,U> return boolean
         *
         * Create map from stars
         */
        BiPredicate<Star, LocalDate> myPredicate = (s, m) -> s.getBirthDate().isBefore(m);
        final List<Star> males2 = stars.stream().filter(star -> {
            return myPredicate.test(star, LocalDate.of(1930, 01, 01));
        }).collect(Collectors.toList());
        males2.stream().forEach(System.out::println);
    }

}
