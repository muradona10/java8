package FunctionalInterfaces.util;

import FunctionalInterfaces.constants.ProjectConstants;
import FunctionalInterfaces.model.Gender;
import FunctionalInterfaces.model.Star;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.stream.Collectors;

public class ListUtils {

    public static List<Star> getList(List<String> strings) {
        return strings.stream().map(s -> {
            String[] data = s.split(ProjectConstants.SEMI_COLUMN);
            String name = data[0];
            Gender gender = Gender.valueOf(data[1]);
            LocalDate dateOfBirth = LocalDate.parse(data[2], DateTimeFormatter.ofPattern(ProjectConstants.DateFormatter_1));
            return new Star(name, gender, dateOfBirth);
        }).collect(Collectors.toList());
    }
}
