package FunctionalInterfaces.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;
import java.util.stream.Stream;

public class FileReaderImpl implements FileReader {

    @Override
    public List<String> readFile(InputStream is) {

        List<String> fileLines = new ArrayList<>();

        try (BufferedReader reader = new BufferedReader(new InputStreamReader(is));
             Stream<String> lines = reader.lines();
             ) {
            lines.forEach(fileLines::add);

            /*
            forEach takes a Consumer as an argument.
            fileLines::add is a Method Reference syntax of this Consumer
            It is also written like this:
            lines.forEach(item -> fileLines.add(item)); or

            Consumer addToListConsumer = i -> fileLines.add(i);
            lines.forEach(addToListConsumer); or

            Consumer<String> addToListConsumer = new Consumer<String>() {
                @Override
                public void accept(String s) {
                    fileLines.add(s);
                }
            };
            lines.forEach(addToListConsumer);
             */
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }

        return fileLines;
    }

}
