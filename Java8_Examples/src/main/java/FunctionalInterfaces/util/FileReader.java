package FunctionalInterfaces.util;

import java.io.InputStream;
import java.util.List;

/**
 * This is a Functional Interface
 */
@FunctionalInterface
public interface FileReader {

    /**
     * Read file via InputStream
     * @param is
     * @return
     */
    List<String> readFile(InputStream is);

}
