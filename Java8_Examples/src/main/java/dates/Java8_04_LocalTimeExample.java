package dates;

import java.time.Clock;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;

public class Java8_04_LocalTimeExample {

    public static void main(String[] args) {

        LocalTime now = LocalTime.now();
        LocalTime max = LocalTime.MAX;
        LocalTime min = LocalTime.MIN;
        LocalTime midnight = LocalTime.MIDNIGHT;

        System.out.println(now); // 15:00:44.221753
        System.out.println(max); // 23:59:59.999999999
        System.out.println(min); // 00:00
        System.out.println(midnight); // 00:00

        System.out.println(LocalTime.now(Clock.systemDefaultZone())); // 15:01:52.606254

        LocalTime time1 = LocalTime.of(11, 45);
        System.out.println(time1); // 11:45

        LocalTime workStartTime = LocalTime.of(9, 00);
        System.out.println(workStartTime); // 09:00
        LocalTime workEndTime = workStartTime.plus(8, ChronoUnit.HOURS);
        System.out.println(workEndTime); // 17:00
        LocalTime bedTime = workEndTime.plusHours(6).plusMinutes(30);
        System.out.println(bedTime); // 23:30


    }

}
