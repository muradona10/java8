package dates;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.temporal.*;

public class Java8_03_DateAdjusterExample {

    public static void main(String[] args) {

        /**
         * We can use Date Adjusters (TemporalAdjusters) to find first date of this year and next year
         * last day of this year and next year, specific day in month etc.
         */

        LocalDate now = LocalDate.now();
        System.out.println(now);

        LocalDate nextSunday = now.with(TemporalAdjusters.next(DayOfWeek.SUNDAY));
        System.out.println(nextSunday);

        LocalDate ofDateAdjuster = now.with(TemporalAdjusters.ofDateAdjuster(l -> l.plus(3, ChronoUnit.MONTHS)));
        System.out.println(ofDateAdjuster);

        LocalDate dayOfWeekInMonth = now.with(TemporalAdjusters.dayOfWeekInMonth(3, DayOfWeek.SUNDAY));
        System.out.println(dayOfWeekInMonth);

        LocalDate firstDayOfMonth = now.with(TemporalAdjusters.firstDayOfMonth());
        System.out.println(firstDayOfMonth);

        LocalDate firstDayOfNextMonth = now.with(TemporalAdjusters.firstDayOfNextMonth());
        System.out.println(firstDayOfNextMonth);

        LocalDate firstDayOfNextYear = now.with(TemporalAdjusters.firstDayOfNextYear());
        System.out.println(firstDayOfNextYear);

        LocalDate firstDayOfYear = now.with(TemporalAdjusters.firstDayOfYear());
        System.out.println(firstDayOfYear);

        LocalDate firstInMonth = now.with(TemporalAdjusters.firstInMonth(DayOfWeek.FRIDAY));
        System.out.println(firstInMonth);

        LocalDate lastDayOfMonth = now.with(TemporalAdjusters.lastDayOfMonth());
        System.out.println(lastDayOfMonth);

        LocalDate lastDayOfYear = now.with(TemporalAdjusters.lastDayOfYear());
        System.out.println(lastDayOfYear);

        LocalDate lastInMonth = now.with(TemporalAdjusters.lastInMonth(DayOfWeek.WEDNESDAY));
        System.out.println(lastInMonth);

        LocalDate nextOrSame = now.with(TemporalAdjusters.nextOrSame(DayOfWeek.SATURDAY));
        System.out.println(nextOrSame);

        LocalDate previous = now.with(TemporalAdjusters.previous(DayOfWeek.THURSDAY));
        System.out.println(previous);

        LocalDate previousOrSame = now.with(TemporalAdjusters.previousOrSame(DayOfWeek.TUESDAY));
        System.out.println(previousOrSame);

    }

}
