package dates;

import java.time.LocalDate;
import java.time.Month;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;

public class Java8_02_LocalDateAndPeriodExample {

    public static void main(String[] args) {

        LocalDate today = LocalDate.now();
        System.out.println(today); // 2020-07-28

        LocalDate myDOB = LocalDate.of(1987, Month.MAY, 25);
        System.out.println(myDOB); // 1987-05-25

        LocalDate myDOB2 = LocalDate.of(1987, 05, 25);
        System.out.println(myDOB2); // 1987-05-25

        LocalDate myDOB3 = LocalDate.parse("25-05-1987", DateTimeFormatter.ofPattern("dd-MM-yyyy"));
        System.out.println(myDOB3); // 1987-05-25

        LocalDate date1 = myDOB.plusMonths(3);
        System.out.println(date1); // 1987-08-25
        System.out.println(myDOB.minusWeeks(3)); // 1987-05-04
        System.out.println(myDOB.plusDays(6)); // 1987-05-31
        System.out.println(myDOB.minusYears(4)); // 1983-05-25

        /**
         * Period is the time difference between dates
         * Period holds small differences like; nanos, millis, seconds, minutes, hours and days
         */

        System.out.println("--------------------- Period Examples ---------------------");
        Period elapsedTime = myDOB.until(today);
        System.out.println(elapsedTime.getChronology()); // ISO
        System.out.println(elapsedTime.getYears()); // 33
        System.out.println(elapsedTime.getMonths()); // 2
        System.out.println(elapsedTime.getDays()); // 3
        System.out.println(elapsedTime.getUnits()); // [Years, Months, Days]

        System.out.println(elapsedTime.get(ChronoUnit.YEARS) + "-" + elapsedTime.get(ChronoUnit.MONTHS) + "-" + elapsedTime.get(ChronoUnit.DAYS));

        long dayCount = myDOB.until(today, ChronoUnit.DAYS);
        System.out.println(dayCount); // 12118
        long monthCount = myDOB.until(today, ChronoUnit.MONTHS);
        System.out.println(monthCount); // 398
        long yearCount = myDOB.until(today, ChronoUnit.YEARS);
        System.out.println(yearCount); // 33
        long decadeCount = myDOB.until(today, ChronoUnit.DECADES);
        System.out.println(decadeCount); // 3


    }

}
