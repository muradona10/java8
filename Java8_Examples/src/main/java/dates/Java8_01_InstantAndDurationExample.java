package dates;

import java.time.Duration;
import java.time.Instant;

public class Java8_01_InstantAndDurationExample {

    public static void main(String[] args) {

        /*
         * INSTANT AND DURATION EXAMPLE
         */
        Instant min = Instant.MIN;
        System.out.println("min: " + min); // 1 billion year ago

        Instant max = Instant.MAX;
        System.out.println("max: " + max); // 31.Dec of the year 1 billion

        Instant epoch = Instant.EPOCH;  // 01.01.1970
        System.out.println("epoch: " + epoch); // 1970-01-01T00:00:00Z

        Instant now = Instant.now();
        System.out.println("now: " + now); // 2020-07-28T10:39:01.510211Z

        /**
         * Duration is the time difference between instances
         * Duration holds small differences like; nanos, millis, seconds, minutes, hours and days
         */
        Instant start = Instant.now();
        for (int i = 0; i < 1000000000 ; i++){
            double random = Math.random() * 100;
        }
        Instant end = Instant.now();
        Duration elapsedTime = Duration.between(start, end);
        System.out.println(elapsedTime.toNanos());
        System.out.println(elapsedTime.toMillis());
        System.out.println(elapsedTime.toSeconds());
        System.out.println(elapsedTime.toMinutes());
        System.out.println(elapsedTime.toHours());
        System.out.println(elapsedTime.toDays());


    }

}
