package dates;

import java.time.*;
import java.util.Set;

public class Java8_05_ZonedTimeExample {

    public static void main(String[] args) {

        /**
         * Java use IANA database to get Zones, https://www.iana.org/time-zones
         */

        final Set<String> availableZoneIds = ZoneId.getAvailableZoneIds();
        availableZoneIds.forEach(System.out::println);

        System.out.println("-----------------------------------------------------------------------------------------");

        /**
         * How to create a ZonedTime
         */
        final ZonedDateTime example1 = ZonedDateTime.of(
                1987, Month.MAY.getValue(), 25,  // year, month, day
                12, 00, 00, 00,  // hour, minute, second, nanoOfSecond
                ZoneId.of("Asia/Istanbul")                        // ZoneId
        );
        System.out.println(example1); // 1987-05-25T12:00+03:00[Asia/Istanbul]

        final ZonedDateTime example2 = ZonedDateTime.of(
                LocalDate.of(1987, Month.MAY, 25),
                LocalTime.of(12, 00, 00, 00),
                ZoneId.of("Europe/Istanbul")
        );
        System.out.println(example2); // 1987-05-25T12:00+03:00[Europe/Istanbul]

        final ZonedDateTime myAge = example1.plus(Period.ofYears(33));
        System.out.println(myAge); // 2020-05-25T12:00+03:00[Asia/Istanbul]

        final ZonedDateTime withZoneSameInstant = example1.withZoneSameInstant(ZoneId.of("Europe/London"));
        System.out.println(withZoneSameInstant); // 1987-05-25T10:00+01:00[Europe/London]
        final ZonedDateTime withZoneSameInstant2 = example1.withZoneSameInstant(ZoneId.of("America/Argentina/Buenos_Aires"));
        System.out.println(withZoneSameInstant2); // 1987-05-25T06:00-03:00[America/Argentina/Buenos_Aires]

    }

}
