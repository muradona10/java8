package dates;

import java.sql.Time;
import java.sql.Timestamp;
import java.time.Instant;
import java.time.LocalDateTime;
import java.util.Date;

public class Java8_07_ConvertAPIandLegacy {

    public static void main(String[] args) {

        Date d1 = Date.from(Instant.now()); // from instant to date
        Instant i1 = (new Date()).toInstant(); // from date to instant

        Timestamp t1 = Timestamp.from(Instant.now()); // from instant to timestamp
        Instant i2 = (Timestamp.valueOf(LocalDateTime.now())).toInstant(); // from timestamp to instant

        System.out.println("d1:" + d1);
        System.out.println("i1:" + i1);
        System.out.println("t1:" + t1);
        System.out.println("i2:" + i2);

        final Date d2 = Time.from(Instant.now());
        System.out.println("d2:" + d2);

    }

}
