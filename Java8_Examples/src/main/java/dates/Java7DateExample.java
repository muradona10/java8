package dates;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class Java7DateExample {

    public static void main(String[] args) {

        Date today = new Date();
        System.out.println(today); // Today - Example: Tue Jul 28 13:18:27 TRT 2020

        Date feb10th2004 = new Date(2004, 1, 10);
        System.out.println(feb10th2004); // Wed Feb 10 00:00:00 TRT 3904

        Calendar cal = Calendar.getInstance();
        cal.set(2004, 01, 10);
        System.out.println(cal.getTime()); //Tue Feb 10 13:20:17 TRT 2004

        cal.add(Calendar.DAY_OF_MONTH, 7); // 7 days later
        Date oneWeekLater = cal.getTime();
        System.out.println(oneWeekLater); // Tue Feb 17 13:22:50 TRT 2004

        Date today2 = new Date();
        SimpleDateFormat sdf1 = new SimpleDateFormat("dd.MM.yyyy");
        System.out.println(sdf1.format(today2)); // 28.07.2020
        SimpleDateFormat sdf2 = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss");
        System.out.println(sdf2.format(today2)); // 28-07-2020 01:31:29

    }

}
