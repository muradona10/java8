package dates;

import java.time.*;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.Locale;

public class Java8_06_FormattingDates {

    public static void main(String[] args) {

        ZonedDateTime myDob = ZonedDateTime.of(LocalDate.of(1987, 05, 25),
                LocalTime.of(12, 15, 00, 00),
                ZoneId.of("Europe/Istanbul"));

        System.out.println(myDob);

        System.out.println(DateTimeFormatter.BASIC_ISO_DATE.format(myDob)); // 19870525+0300
        System.out.println(DateTimeFormatter.ISO_DATE.format(myDob)); // 1987-05-25+03:00
        System.out.println(DateTimeFormatter.ISO_DATE_TIME.format(myDob)); // 1987-05-25T12:15:00+03:00[Europe/Istanbul]
        System.out.println(DateTimeFormatter.ISO_INSTANT.format(myDob)); // 1987-05-25T09:15:00Z
        System.out.println(DateTimeFormatter.ISO_LOCAL_DATE.format(myDob)); // 1987-05-25
        System.out.println(DateTimeFormatter.ISO_LOCAL_DATE_TIME.format(myDob)); // 1987-05-25T12:15:00
        System.out.println(DateTimeFormatter.ISO_LOCAL_TIME.format(myDob)); // 12:15:00
        System.out.println(DateTimeFormatter.ISO_OFFSET_DATE.format(myDob)); // 1987-05-25+03:00
        System.out.println(DateTimeFormatter.ISO_OFFSET_DATE_TIME.format(myDob)); // 1987-05-25T12:15:00+03:00
        System.out.println(DateTimeFormatter.ISO_OFFSET_TIME.format(myDob)); // 12:15:00+03:00
        System.out.println(DateTimeFormatter.ISO_ORDINAL_DATE.format(myDob)); // 1987-145+03:00
        System.out.println(DateTimeFormatter.ISO_TIME.format(myDob)); // 12:15:00+03:00
        System.out.println(DateTimeFormatter.ISO_WEEK_DATE.format(myDob)); // 1987-W22-1+03:00
        System.out.println(DateTimeFormatter.ISO_ZONED_DATE_TIME.format(myDob)); // 1987-05-25T12:15:00+03:00[Europe/Istanbul]
        System.out.println(DateTimeFormatter.RFC_1123_DATE_TIME.format(myDob)); // Mon, 25 May 1987 12:15:00 +0300

        System.out.println("----------------------------------------------------------------------------------------");

        System.out.println(DateTimeFormatter.ofPattern("dd-MM-yyyy").format(LocalDate.now())); //28-07-2020
        System.out.println(DateTimeFormatter
                .ofPattern("dd.MM.yyyy hh:mm:ss", Locale.CANADA).format(LocalDateTime.now())); // 28.07.2020 07:49:20

        System.out.println(DateTimeFormatter.ofLocalizedTime(FormatStyle.FULL).format(ZonedDateTime.now())); // 19:54:01 Turkey Time
        System.out.println(DateTimeFormatter.ofLocalizedTime(FormatStyle.LONG).format(ZonedDateTime.now())); // 19:54:01 TRT
        System.out.println(DateTimeFormatter.ofLocalizedTime(FormatStyle.SHORT).format(LocalTime.now())); // 19:54
        System.out.println(DateTimeFormatter.ofLocalizedTime(FormatStyle.MEDIUM).format(LocalTime.now())); // 19:54:22

        System.out.println("----------------------------------------------------------------------------------------");

        System.out.println(DateTimeFormatter.ofPattern("E").format(LocalDate.now())); // Tue
        System.out.println(DateTimeFormatter.ofPattern("EEEE").format(LocalDate.now())); // Tuesday


    }

}
