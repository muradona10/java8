package streams.collectors;

import FunctionalInterfaces.model.Gender;
import FunctionalInterfaces.model.Star;
import FunctionalInterfaces.util.FileReader;
import FunctionalInterfaces.util.FileReaderImpl;
import FunctionalInterfaces.util.ListUtils;

import java.io.InputStream;
import java.time.Month;
import java.util.List;
import java.util.Map;
import java.util.TreeSet;
import java.util.stream.Collectors;

public class CollectorsExample {

    public static void main(String[] args) {

        FileReader reader = new FileReaderImpl();
        InputStream is = CollectorsExample.class.getClassLoader().getResourceAsStream("YesilcamYildizlari.txt");
        List<String> lines = reader.readFile(is);

        List<Star> stars = ListUtils.getList(lines);

        /*
         * Example 1 : JOINING
         * Collect stars names in a String with joining function
         */
        String nameString = stars.stream().map(Star::getName).collect(Collectors.joining(", ", "[", "]"));
        System.out.println(nameString);

        System.out.println("-----------------------------------------------------------------------------------------");

        /*
         * Example 2 : TOLIST
         * Collect female stars in a List
         */
        List<Star> femaleStarList = stars.stream().filter(star -> star.getGender().equals(Gender.F)).collect(Collectors.toList());
        femaleStarList.forEach(System.out::println);

        System.out.println("-----------------------------------------------------------------------------------------");

        /*
         * Example 3 : GROUPING BY
         * Collect all stars group by their birth date months
         */
        Map<Month, List<Star>> monthListMap = stars.stream().collect(Collectors.groupingBy(s -> s.getBirthDate().getMonth()));
        monthListMap.entrySet().forEach(System.out::println);
        System.out.println("-----------------------------------------------------------------------------------------");

        /*
          Example 4: GROUPING BY, COUNTING (downstream collector)
          Collect all stars group by their birth date year and count them
         */
        Map<Integer, Long> yearMap = stars.stream().collect(Collectors.groupingBy(s -> s.getBirthDate().getYear(), Collectors.counting()));
        yearMap.entrySet().forEach(System.out::println);
        System.out.println("-----------------------------------------------------------------------------------------");

        /*
          Example 6: GROUPING BY, MAPPING (downstream collector)
          Collect all stars group by their birth date month and create a map with month and name List key value pair
         */
        Map<Month, List<String>> nameListMap = stars.stream()
                .collect(Collectors.groupingBy(           // group by
                        s -> s.getBirthDate().getMonth(), // their birth date month
                        Collectors.mapping(               // create new map with keys are months
                                Star::getName,            // values are name of star
                                Collectors.toList())));// create list with this names
        nameListMap.entrySet().forEach(System.out::println);
        System.out.println("-----------------------------------------------------------------------------------------");

        /*
          Example 7: GROUPING BY, MAPPING (downstream collector)
          Collect all stars group by their birth date month and create a map with month and name Tree Set key value pair
          Tree Set is holds alphabetically sorted here
         */
        Map<Month, TreeSet<String>> treeSetMap = stars.stream()
                .collect(Collectors.groupingBy(           // group by
                        s -> s.getBirthDate().getMonth(), // their birth date month
                        Collectors.mapping(               // create new map with keys are months
                                Star::getName,            // values are name of star
                                Collectors.toCollection(TreeSet::new))));// create new TreeSet with this names
        treeSetMap.entrySet().forEach(System.out::println);
        System.out.println("-----------------------------------------------------------------------------------------");

        /*
          Example 8: GROUPING BY, MAPPING (downstream collector)
          Collect all stars group by their GENDER and create a map with gender and name JOINING key value pair
         */
        Map<Gender, String> stringMap = stars.stream()
                .collect(Collectors.groupingBy(           // group by
                        Star::getGender,                  // their gender
                        Collectors.mapping(               // create new map with keys are gender of star
                                Star::getName,            // values are name of star
                                Collectors.joining(", ", ">>> ", " <<<"))));// create with this names
        stringMap.entrySet().forEach(System.out::println);
        System.out.println("-----------------------------------------------------------------------------------------");

    }

}
