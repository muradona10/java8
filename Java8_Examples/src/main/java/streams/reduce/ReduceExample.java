package streams.reduce;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.function.Predicate;

public class ReduceExample {

    public static void main(String[] args) {

        /*
         * Example 1 : Already available reductions
         * max, min, count
         */

        List<Integer> myInts = List.of(45, 65, 564, 22, 11, 987,3454, 75, 68, 90,91, 17);
        Optional<Integer> max = myInts.stream().max(Integer::compareTo);
        Optional<Integer> min = myInts.stream().min(Integer::compareTo);
        long count = myInts.stream().count();
        if(max.isPresent()){
            System.out.println(max.get());
        }
        if(min.isPresent()){
            System.out.println(min.get());
        }
        System.out.println(count);

        System.out.println("-----------------------------------------------------------------------------------------");
        max.ifPresent(System.out::println);
        min.ifPresent(System.out::println);
        System.out.println("-----------------------------------------------------------------------------------------");


        /*
         * Example 2
         * Reduce function
         */
        final Integer sum = myInts.stream().reduce(0, Integer::sum);
        final Integer multiply = myInts.stream().limit(4).reduce(1, (i1, i2) -> i1 * i2);

        long average = sum / count;

        System.out.println(sum);
        System.out.println(multiply);
        System.out.println(average);
        System.out.println("-----------------------------------------------------------------------------------------");

        /*
         * Example 3 : Reductions that return Optionals
         * findFirst, findAny
         */
        Predicate<Integer> mod10is7 = i -> i % 10 == 7;

        Optional<Integer> first = myInts.stream().filter(mod10is7).findFirst();
        System.out.println(first.orElseGet(() -> 0));

        Optional<Integer> any = myInts.stream().parallel().filter(mod10is7).findAny();
        System.out.println(any.orElseThrow(() -> new NoSuchElementException("No Element")));

        try {
            System.out.println(myInts.stream().filter(i -> i < 0).findFirst().orElseThrow(() -> new NoSuchElementException("No Element")));
        } catch (NoSuchElementException ex) {
            System.out.println(ex.getMessage());
        }

        System.out.println("-----------------------------------------------------------------------------------------");

        /*
         * Example 4 : Reductions that returns boolean
         * allMatch, anyMatch, noneMatch
         */
        boolean anyMatch = myInts.stream().anyMatch(i -> i > 500);
        System.out.println(anyMatch);

        boolean allMatch = myInts.stream().allMatch(i -> i < 5000);
        System.out.println(allMatch);

        boolean noneMatch = myInts.stream().noneMatch(i -> i < 0);
        System.out.println(noneMatch);

        boolean allMatch2 = myInts.stream().allMatch(i -> i > 100);
        System.out.println(allMatch2);


    }

}
