package streams.optionals;

import java.util.Comparator;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

public class OptionalExample {

    public static void main(String[] args) {

        List<Integer> myInts = List.of(45, 65, 564, 22, 11, 987,3454, 75, 68, 90,91, 17);

        Optional<Integer> min = myInts.stream().min(Comparator.naturalOrder());
        Optional<Integer> max = myInts.stream().max(Comparator.naturalOrder());

        System.out.println("- isPresent Example");
        if(min.isPresent()){
            System.out.println(min.get());
        }

        if(max.isPresent()){
            System.out.println(max.get());
        }

        System.out.println("- ifPresent Example");
        min.ifPresent(System.out::println);
        max.ifPresent(System.out::println);

        System.out.println("- orElse, orElseGet, orElseThrow Example");
        Optional<String> defaultString = Optional.of("Default");
        Optional<String> exampleString = Optional.of("Example");
        Optional<String> empty = Optional.empty();

        String s1 = exampleString.orElse(defaultString.get());
        System.out.println(s1);
        String s2 = empty.orElseGet(() -> defaultString.get()); // Lazy because of Lambda Expression
        System.out.println(s2);
        String s3 = empty.orElse(defaultString.get());
        System.out.println(s3);

        try {
            String s4 = empty.orElseThrow(NoSuchElementException::new); // Lazy Construct
            System.out.println(s4);
        } catch (NoSuchElementException ex) {
            System.out.println(ex.getMessage());
        }


    }

}
