package streams.mapandflatmap;

import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class MapAndFlatMapExample {

    public static void main(String[] args) {

        final List<String> turkishTeams = List.of("Fenerbahçe","Galatasaray","Beşiktaş","Trabzonspor");
        final List<String> englishTeams = List.of("Mancherster United", "Liverpool", "Chelsea", "Arsenal");
        final List<String> spannishTeams = List.of("Real Madrid", "Barcelona", "Athletico Madrid", "Sevilla");
        final List<String> itallianTeams = List.of("Milan", "Juventus", "Inter", "Roma");

        List<List<String>> allTeams = List.of(turkishTeams, englishTeams, spannishTeams, itallianTeams);

        final List<Stream<String>> collect1 = allTeams.stream()
                .map(l -> l.stream().filter(s -> s.length() > 8))
                .collect(Collectors.toList());

        collect1.stream().map(stringStream -> {
            return stringStream.collect(Collectors.toList());
        }).forEach(System.out::println);

        System.out.println("_________________________________________________________________________________________");

        /*
         * Flatmap used for combine List of Lists
         */
        Function<List<String>, Stream<String>> flatMapper = l -> l.stream();
        final List<String> collect2 = allTeams.stream().flatMap(flatMapper).collect(Collectors.toList());

        collect2.stream().forEach(System.out::println);


    }

}
