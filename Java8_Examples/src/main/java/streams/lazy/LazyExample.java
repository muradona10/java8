package streams.lazy;

import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class LazyExample {

    public static void main(String[] args) {

        Stream<Integer> myInts = IntStream.range(1,500).boxed();

        /*
         * This writes nothing because peek is not a terminate operator for streams
         * and streams can hold nothing
         */
        myInts.peek(System.out::println);

        /*
         * This writes integers from 1 to 500 because we add a terminate operator
         * for stream. collect() is one of the terminate operator like foreach(), min(), max() etc.
         */
        myInts.peek(System.out::println).collect(Collectors.toList());

    }

}
